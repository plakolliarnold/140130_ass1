package arnold.plakolli.com.xmaps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.SQLException;

public class Database_Helper {

    //Declaration of variables needed for creating the database
    private static final String KEY_ROWID = "id";
    private static final String KEY_NAME = "cityCountryName";

    private static final String DATABASE_NAME = "PlacesDB";
    private static final String DATABASE_TABLE = "cityCountryTable";
    private static final int DATABASE_VERSION = 1;

    private DbHelper helper;
    private final Context context;
    private SQLiteDatabase database;

    private class DbHelper extends SQLiteOpenHelper {

        //The constructor of this class creates on declaration of the object of this class the datbase with the name stores in the variable DATABASE_NAME
        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(
                    "CREATE TABLE " + DATABASE_TABLE + " (" +
                            KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            KEY_NAME + " TEXT NOT NULL);"
            );
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }
    }

    public Database_Helper(Context ct) {
        context = ct;
    }

    //This method creates the database, gets that SQL database and assigns it to the SQLiteDatabase variable declared above
    public Database_Helper open() throws SQLException {
        helper = new DbHelper(context);
        database = helper.getWritableDatabase();
        return this;
    }

    public void close() {
        helper.close();
    }

    //This method is used for inserting a string into the database table
    public void createEntry(String cityCountry) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, cityCountry);
        database.insert(DATABASE_TABLE, null, cv);
    }

    //Using this method you get a string containg all the string data saved in the database
    public String getData() {
        String[] columns = new String[]{KEY_ROWID, KEY_NAME};
        Cursor c = database.query(true, DATABASE_TABLE, columns, null, null, KEY_NAME, null, null, null);
        String result = "";

        int i = 0;
        int iName = c.getColumnIndex(KEY_NAME);

        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            i++;
            result = result + "   " + i + "   " + c.getString(iName) + "\n\n";
        }

        return result;
    }
}
