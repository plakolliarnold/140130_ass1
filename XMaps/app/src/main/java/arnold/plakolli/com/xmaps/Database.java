package arnold.plakolli.com.xmaps;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class Database extends Activity {

    private TextView tvCityCountryNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.database);

        tvCityCountryNames = (TextView) findViewById(R.id.tvCityCountryNames);

        try {
            Database_Helper places = new Database_Helper(this);

            //Gets the data from the database and assigns those data to a string variable which is then used to display those data in the screen
            places.open();
            String data = places.getData();
            places.close();
            tvCityCountryNames.setText(data);
        }catch (Exception e){
            Toast.makeText(this,e.toString(),Toast.LENGTH_SHORT).show();
        }
    }
}
