package arnold.plakolli.com.xmaps;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class Info extends Activity {

    private TextView tvLatitudeValue, tvLongitudeValue, tvTemperatureValue, tvHumidityValue, tvCityCountry;
    private String latitude, longitude, temperature, humidity, cityCountry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info);

        Initialize();

        Intent i = getIntent();

        //Gets the data from the Main activity and assigns those values to the variables defined
        latitude = i.getStringExtra("locationLatitude");
        longitude = i.getStringExtra("locationLongitude");
        temperature = i.getStringExtra("locationTemperature");
        humidity = i.getStringExtra("locationHumidity");
        cityCountry = i.getStringExtra("cityCountry");


        //It fills the TextViews with the data received from the Main activity
        tvLatitudeValue.setText(latitude);
        tvLongitudeValue.setText(longitude);
        tvTemperatureValue.setText(temperature);
        tvHumidityValue.setText(humidity);
        tvCityCountry.setText(cityCountry);
    }

    //Initializing of some variables
    private void Initialize() {
        tvLatitudeValue = (TextView) findViewById(R.id.tvLatitudeValue);
        tvLongitudeValue = (TextView) findViewById(R.id.tvLongitudeValue);
        tvTemperatureValue = (TextView) findViewById(R.id.tvTemperatureValue);
        tvHumidityValue = (TextView) findViewById(R.id.tvHumidityValue);
        tvCityCountry = (TextView) findViewById(R.id.tvCityCountry);
    }

    //These next two methods are used to store and restore data when changing device orientation
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //It stores the data and assigns everyone of them their own key string
        //Bundle.putString(key, value)
        outState.putString("LATITUDE", latitude);
        outState.putString("LONGITUDE", longitude);
        outState.putString("TEMPERATURE", temperature);
        outState.putString("HUMIDITY", humidity);
        outState.putString("CITYCOUNTRY", cityCountry);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //It restores the data using their key string and assigns the values stores there to some variables
        //variable = Bundle.getString(key);
        latitude = savedInstanceState.getString("LATITUDE");
        longitude = savedInstanceState.getString("LONGITUDE");
        temperature = savedInstanceState.getString("TEMPERATURE");
        humidity = savedInstanceState.getString("HUMIDITY");
        cityCountry = savedInstanceState.getString("CITYCOUNTRY");
    }
}
