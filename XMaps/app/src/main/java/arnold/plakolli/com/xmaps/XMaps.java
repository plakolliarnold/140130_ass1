package arnold.plakolli.com.xmaps;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.location.GpsSatellite;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

public class XMaps extends Activity implements View.OnClickListener {

    private GoogleMap map;
    private EditText etCity;
    private Button bSearch, bDatabase, bInfo;
    private Geocoder gc2;
    private String locationLatitude , locationLongitude, locationTemperature, locationHumidity, cityCountryDB;
    private String url = "http://api.openweathermap.org/data/2.5/weather?q=";
    private String n_a = "n/a";
    private String searchValue;
    private HandleJSON obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);

        Initialize();
        bDatabase.setEnabled(true);

        map.setMyLocationEnabled(true);

        bSearch.setOnClickListener(this);
        bDatabase.setOnClickListener(this);
        bInfo.setOnClickListener(this);
    }

    //This method as the name itself explains is used for initializing some variables
    private void Initialize() {
        etCity = (EditText) findViewById(R.id.etCity);
        bSearch = (Button) findViewById(R.id.bSearch);
        bDatabase = (Button) findViewById(R.id.bDatabase);
        bInfo = (Button) findViewById(R.id.bInfo);
        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        gc2 = new Geocoder(this);
        locationLongitude = n_a;
        locationLatitude = n_a;
        locationTemperature = n_a;
        locationHumidity = n_a;
    }

    //This method converts the format the longitude and latitude are displayed
    public String convertToDMS(double DEG_Value) {
        String result = "D°";

        int D = (int) DEG_Value;
        double restOfValue1 = DEG_Value - D;

        int M = (int) (restOfValue1 * 60);
        double restOfValue2 = restOfValue1 - M;

        int S = (int) (restOfValue2 * 60);

        result = D + "° " + M + "' " + S + "''";

        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bSearch:
                //The "searchValue" variable is attached to the already defined url above to search for the data
                searchValue = etCity.getText().toString();

                //Hides virtual keyboard on Search button click
                InputMethodManager imm = (InputMethodManager) getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etCity.getWindowToken(), 0);

                try {

                    List<Address> list = gc2.getFromLocationName(etCity.getText().toString(), 1);
                    if (!list.isEmpty()) {

                        //Finds the location the user searched for and zooms to that location
                        final Address address = list.get(0);
                        final LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
                        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(latLng, 7);
                        map.animateCamera(update);

                        String finalUrl = url + searchValue;

                        //Updates EditText with official city or country name
                        if (address.getCountryName() != null && address.getLocality() != null) {
                            etCity.setText(address.getLocality() + ", " + address.getCountryName());
                        } else if (address.getCountryName() == null) {
                            etCity.setText(address.getLocality());
                        } else if (address.getLocality() == null) {
                            etCity.setText(address.getCountryName());
                        }

                        cityCountryDB = etCity.getText().toString();

                        //Creates an entry in the database with the official city and country name

                        Database_Helper entry = new Database_Helper(this);

                        try {
                            entry.open();
                            entry.createEntry(cityCountryDB);
                            entry.close();
                            Toast.makeText(this, "Successfully registered", Toast.LENGTH_SHORT).show();
                        } catch (SQLException e) {
                            Toast.makeText(this, "Could not register", Toast.LENGTH_SHORT).show();
                        }

                        //Gets the data that will be sent to the Info activity

                        obj = new HandleJSON(finalUrl);
                        obj.fetchData();

                        locationTemperature = obj.getTemperature();
                        locationHumidity = obj.getHumidity();
                        locationLatitude = convertToDMS(address.getLatitude());
                        locationLongitude = convertToDMS(address.getLongitude());

                        bInfo.setEnabled(true);
                    } else {
                        Toast t = Toast.makeText(this, "Location not found", Toast.LENGTH_SHORT);
                        t.show();
                    }
                } catch (IOException e) {
                    Toast t = Toast.makeText(this, "Server not responding", Toast.LENGTH_SHORT);
                    t.show();
                } finally {
                    bSearch.clearFocus();
                }
                break;

            case R.id.bDatabase:
                Intent i1 = new Intent("com.plakolli.arnold.DATABASE");
                startActivity(i1);
                break;

            case R.id.bInfo:
                //Checks if the input field is empty so that it won't display empty data in the Info activity

                if (!etCity.getText().toString().isEmpty()) {
                    Intent i2 = new Intent("com.plakolli.arnold.INFO");

                    //Sends the data to the Info activity

                    i2.putExtra("locationLatitude", locationLatitude);
                    i2.putExtra("locationLongitude", locationLongitude);
                    i2.putExtra("locationTemperature", locationTemperature);
                    i2.putExtra("locationHumidity", locationHumidity);
                    i2.putExtra("cityCountry", cityCountryDB);
                    startActivity(i2);
                } else {
                    Toast.makeText(this, "Please enter a location to search for", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}