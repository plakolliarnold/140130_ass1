package arnold.plakolli.com.xmaps;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;

public class HandleJSON {
    private String temperature = "n/a";
    private String humidity = "n/a";
    private String urlString = null;

    public HandleJSON(String url) {
        this.urlString = url;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() { return humidity; }

    //This method searches for values using JSON in the input string variable
    public void readAndParseData(String in) {
        try {
            JSONObject reader = new JSONObject(in);

            JSONObject main = reader.getJSONObject("main");

            double temp = main.getDouble("temp");
            DecimalFormat form = new DecimalFormat("0.00"); //Used to change the format of temperature and humidity
            temperature = form.format(temp - 273.15);
            temperature = temperature + " °C";

            temp = main.getDouble("humidity");
            form = new DecimalFormat("0");
            humidity = form.format(temp);
            humidity = humidity + " %";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fetchData() {
        //The thread below deals with getting the stream from the specific url, converts that stream to a string
        //And then searches for values on that string data using the readAndParseData method
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(urlString);
                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                    conn.setRequestMethod("GET");
                    conn.setDoInput(true);
                    conn.setDoOutput(true);
                    conn.connect();

                    InputStream stream = conn.getInputStream();
                    String data = convertStreamToString(stream);

                    readAndParseData(data);

                    stream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        thread.start();

        try{
            thread.join(5000);
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    //Simply converts the input stream and into a tring and returns that string
    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

}
